use std::io::Cursor;

use super::*;
use crate::{StreamDeserializer, StreamSerializer};

fn test_serialization_success(value: &HaxeValue, serialized: &[u8]) {
    let mut ser = StreamSerializer::new(Cursor::new(Vec::new()), SerializationCache::new());
    ser.serialize(value).unwrap();
    let out = ser.into_inner().into_inner();
    if &out[..] != serialized {
        panic!(
            "serialization output is incorrect:\n  output: {}\nexpected: {}",
            String::from_utf8_lossy(&out),
            String::from_utf8_lossy(serialized),
        )
    }
}

fn test_deserialization_success(value: &HaxeValue, serialized: &[u8]) {
    let mut de = StreamDeserializer::new(Cursor::new(serialized), DeserializationCache::new());
    let deserialized: HaxeValue = de.deserialize().unwrap();
    de.end().unwrap();
    let output = format!("{:#?}", deserialized);
    let expected = format!("{:#?}", value);
    if output != expected {
        panic!(
            "deserialization output is incorrect:\n  output: {}\nexpected: {}",
            output, expected
        );
    }
}

macro_rules! test_value_success {
    ($name:ident : $value:expr => $out:expr) => {
        #[test]
        fn $name() {
            let value: HaxeValue = {
                #[allow(unused_imports)]
                use HaxeObject::*;
                #[allow(unused_imports)]
                use HaxeValue::*;
                $value
            };
            let serialized: &[u8] = $out;
            test_serialization_success(&value, serialized);
            test_deserialization_success(&value, serialized);
        }
    };
}

macro_rules! map_lit {
    ($($key:expr => $value:expr),* $(,)?) => {{
        #[allow(unused_mut)] let mut map = indexmap::IndexMap::new();
        $(map.insert($key, $value);)*
        map
    }}
}

test_value_success! { primitives:
    Array(vec![
        Null,
        Bool(true), Bool(false),
        Int(0), Int(10), Int(-10),
        Float(3e50), Float(-3.1415), Float(f64::NAN),
        Float(f64::INFINITY), Float(f64::NEG_INFINITY),
    ]).into() => b"antfzi10i-10d3e50d-3.1415kpmh"
}

test_value_success! { escaped_string:
    String("Manger toutes les pièces noires".into())
    => b"y44:Manger%20toutes%20les%20pi%C3%A8ces%20noires"
}

test_value_success! { string_references:
    Array(vec![
        String("foo".into()), String("bar".into()),
        String("baz".into()), String("bar".into()),
        String("qux".into()), String("foo".into()),
    ]).into() => b"ay3:fooy3:bary3:bazR1y3:quxR0h"
}

test_value_success! { array_nulls:
    Array(vec![
        Int(99), Null, Null, Int(99), Null, Int(99), Null
    ]).into() => b"ai99u2i99ni99nh"
}

test_value_success! { misc_objects:
    Array(vec![
        ClassDef("Class".into()), EnumDef("Enum".into()),
        Date(
            chrono::NaiveDate::from_ymd_opt(2020, 2, 29)
            .and_then(|d| d.and_hms_opt(12, 34, 56))
            .and_then(HaxeDate::try_new)
            .unwrap()
        ).into(),
        Exception(String("Exception".into())).into(),
    ]).into() => b"aAy5:ClassBy4:Enumv2020-02-29 12:34:56xy9:Exceptionh"
}

#[test]
fn test_date_from_timestamp() {
    let timestamps = &[0, -99999999, 99999999, i32::MIN.into(), i32::MAX.into()];
    for timestamp in timestamps {
        let serialized = format!("v{}", timestamp);
        let value =
            HaxeValue::Object(HaxeObject::Date(HaxeDate::from_timestamp(*timestamp)).into());
        test_deserialization_success(&value, serialized.as_bytes());
    }
}

test_value_success! { byte_objects: {
    fn to_vec(bytes: &[u8]) -> Vec<u8> { Vec::from(bytes) }

    Array(vec![
        Bytes(to_vec(b"")).into(),
        Bytes(to_vec(b"abc")).into(),
        Bytes(to_vec(b"defg")).into(),
        Bytes(to_vec(b"hijkl")).into(),
        Bytes(to_vec(b"mnopqr")).into(),
    ]).into()} => b"as0:s4:YWJjs6:ZGVmZws7:aGlqa2ws8:bW5vcHFyh"
}

test_value_success! { compound_objects:
    Array(vec![
        List(vec![Null, Null]).into(),
        StringMap(map_lit!(
            "a".into() => Int(1), "b".into() => Int(2), "c".into() => Int(3),
        )).into(),
        IntMap(map_lit!(
            1 => Int(10), 2 => Int(20), 3 => Int(30),
        )).into(),
        ObjectMap(map_lit!(
            Struct(map_lit!()).into() => Int(1),
            Struct(map_lit!()).into() => Int(2),
        )).into(),
        Struct(map_lit!(
            "d".into() => Int(4), "e".into() => Int(5), "f".into() => Int(6),
        )).into(),
        Class {
            name: "Main".into(),
            fields: map_lit!("foo".into() => Int(42), "bar".into() => Int(-42)),
        }.into(),
        EnumByName {
            name: "haxe.ds.Option".into(),
            tag: "Some".into(),
            params: vec![Int(100)],
        }.into(),
        EnumByIndex {
            name: "haxe.ds.Option".into(),
            tag: 0,
            params: vec![Int(101)],
        }.into(),
        Custom {
            name: "Custom".into(),
            contents: vec![Int(99)],
        }.into(),
    ]).into() => b"alnnhby1:ai1y1:bi2y1:ci3hq:1i10:2i20:3i30hMogi1ogi2hoy1:di4y1:ei5y1:fi6g\
        cy4:Mainy3:fooi42y3:bari-42gwy14:haxe.ds.Optiony4:Some:1i100jR9:0:1i101Cy6:Customi99gh"
}
