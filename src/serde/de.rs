use std::marker::PhantomData;

use serde::de::{
    self, Deserialize as SerdeDeserialize, DeserializeSeed as SerdeDeserializeSeed,
    Deserializer as SerdeDeserializer, IntoDeserializer as _, Visitor as SerdeVisitor,
};

use super::adapter::*;
use crate::raw::de::{self as raw, HaxeDeserialize, HaxeDeserializer, HaxeVisitor, StringWithRef};
use crate::raw::{ser::StringArg, HaxeDate, HaxeError, ObjectRef, Result};
use crate::value::DeserializationCache;

pub struct Deserializer<'a, D> {
    de: D,
    ctx: &'a mut DeserializerContext,
}

pub struct DeserializerContext {
    cache: DeserializationCache,
}

impl Default for DeserializerContext {
    fn default() -> Self {
        Self::new()
    }
}

pub struct DeserializeSeed<'a, S> {
    ctx: &'a mut DeserializerContext,
    seed: S,
}

impl<'a, 'de, T> HaxeDeserialize<'de, &'a mut DeserializerContext> for T
where
    T: SerdeDeserialize<'de> + ?Sized,
{
    fn deserialize<D: HaxeDeserializer<'de>>(
        state: &'a mut DeserializerContext,
        deserializer: D,
    ) -> Result<Self> {
        let de = Deserializer {
            de: deserializer,
            ctx: state,
        };
        <T as SerdeDeserialize<'de>>::deserialize(de)
    }
}

impl<'a, 'de, S> HaxeDeserialize<'de, DeserializeSeed<'a, S>> for S::Value
where
    S: SerdeDeserializeSeed<'de>,
{
    fn deserialize<D: HaxeDeserializer<'de>>(
        state: DeserializeSeed<'a, S>,
        deserializer: D,
    ) -> Result<Self> {
        let de = Deserializer {
            de: deserializer,
            ctx: state.ctx,
        };
        <S as SerdeDeserializeSeed<'de>>::deserialize(state.seed, de)
    }
}

impl DeserializerContext {
    pub fn new() -> Self {
        DeserializerContext {
            cache: DeserializationCache::new(),
        }
    }

    pub fn with<'a, 'de, D>(&'a mut self, deserializer: D) -> Deserializer<'a, D>
    where
        D: HaxeDeserializer<'de>,
    {
        Deserializer {
            de: deserializer,
            ctx: self,
        }
    }

    pub fn with_seed<'a, 'de, S>(&'a mut self, seed: S) -> DeserializeSeed<'a, S>
    where
        S: SerdeDeserializeSeed<'de>,
    {
        DeserializeSeed { seed, ctx: self }
    }

    fn get_string(&mut self, (string, str_ref): StringWithRef) -> Result<&str> {
        // TODO: find a way to get a &'de str, so we can call visit_borrowed_str
        if let Some(s) = string {
            self.cache.put_string(s, str_ref);
        }
        self.cache.get_string(str_ref)
    }
}

impl<'a, 'de, D> SerdeDeserializer<'de> for Deserializer<'a, D>
where
    D: HaxeDeserializer<'de>,
{
    type Error = HaxeError;
    serde::forward_to_deserialize_any!(
        bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf unit unit_struct seq tuple
        tuple_struct map struct identifier ignored_any
    );

    fn deserialize_any<V: SerdeVisitor<'de>>(self, visitor: V) -> Result<V::Value> {
        <() as VisitorKind>::deserialize_with(self.ctx, self.de, visitor)
    }

    fn deserialize_option<V: SerdeVisitor<'de>>(mut self, visitor: V) -> Result<V::Value> {
        if self.de.peek_null()? {
            let state = self.ctx.with_seed(std::marker::PhantomData);
            let _ = <de::IgnoredAny as HaxeDeserialize<_>>::deserialize(state, self.de)?;
            visitor.visit_none()
        } else {
            visitor.visit_some(self.ctx.with(self.de))
        }
    }

    fn deserialize_enum<V: SerdeVisitor<'de>>(
        self,
        name: &'static str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value> {
        self.de.deserialize_any(EnumVisitor {
            ctx: self.ctx,
            vis: visitor,
            name,
            fields,
        })
    }

    fn deserialize_newtype_struct<V: SerdeVisitor<'de>>(
        self,
        name: &'static str,
        visitor: V,
    ) -> Result<V::Value> {
        use AdapterMarker::*;

        fn with_adapter<'de, K, V, D>(this: Deserializer<'_, D>, visitor: V) -> Result<V::Value>
        where
            K: VisitorKind,
            V: SerdeVisitor<'de>,
            D: HaxeDeserializer<'de>,
        {
            let de = AdapterDeserializer {
                de: this.de,
                ctx: this.ctx,
                _kind: PhantomData::<K>,
            };
            visitor.visit_newtype_struct(de)
        }

        match AdapterMarker::parse(name) {
            None => with_adapter::<(), _, _>(self, visitor),
            Some(Date) => with_adapter::<DateAdapter<()>, _, _>(self, visitor),
            Some(Array) => with_adapter::<ArrayAdapter<()>, _, _>(self, visitor),
            Some(List) => with_adapter::<ListAdapter<()>, _, _>(self, visitor),
            Some(Struct) => with_adapter::<StructAdapter<()>, _, _>(self, visitor),
            Some(StringMap) => with_adapter::<StringMapAdapter<()>, _, _>(self, visitor),
            Some(IntMap) => with_adapter::<IntMapAdapter<()>, _, _>(self, visitor),
            Some(ObjectMap) => with_adapter::<ObjectMapAdapter<()>, _, _>(self, visitor),
            Some(Class) => visitor.visit_newtype_struct(ClassDeserializer {
                ctx: self.ctx,
                de: self.de,
            }),
            Some(ClassDef) => with_adapter::<ClassDefAdapter<()>, _, _>(self, visitor),
            Some(EnumDef) => with_adapter::<EnumDefAdapter<()>, _, _>(self, visitor),
            Some(Custom) => Err(HaxeError::unrepresentable(
                "custom Haxe serialization isn't supported",
            )),
            Some(Exception) => with_adapter::<ExceptionAdapter<()>, _, _>(self, visitor),
            Some(Unknown) => Err(HaxeError::unrepresentable(format!(
                "unknown adapter type: {}",
                &name[1..]
            ))),
        }
    }
}

struct AdapterDeserializer<'a, D, K> {
    de: D,
    ctx: &'a mut DeserializerContext,
    _kind: PhantomData<K>,
}

impl<'a, 'de, D, K> SerdeDeserializer<'de> for AdapterDeserializer<'a, D, K>
where
    D: HaxeDeserializer<'de>,
    K: VisitorKind,
{
    type Error = HaxeError;
    serde::forward_to_deserialize_any!(
        bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf unit unit_struct seq tuple
        tuple_struct map struct enum identifier ignored_any
    );

    fn deserialize_any<V: SerdeVisitor<'de>>(self, visitor: V) -> Result<V::Value> {
        <K as VisitorKind>::deserialize_with(self.ctx, self.de, visitor)
    }

    fn deserialize_option<V: SerdeVisitor<'de>>(mut self, visitor: V) -> Result<V::Value> {
        if self.de.peek_null()? {
            let state = self.ctx.with_seed(std::marker::PhantomData);
            let _ = <de::IgnoredAny as HaxeDeserialize<_>>::deserialize(state, self.de)?;
            visitor.visit_none()
        } else {
            visitor.visit_some(self)
        }
    }

    fn deserialize_newtype_struct<V: SerdeVisitor<'de>>(
        self,
        name: &'static str,
        visitor: V,
    ) -> Result<V::Value> {
        match AdapterMarker::parse(name) {
            None => visitor.visit_newtype_struct(self),
            Some(adapter) => Err(HaxeError::unrepresentable(format!(
                "invalid nested adapter: {:?}",
                adapter
            ))),
        }
    }
}

struct ClassDeserializer<'a, D> {
    de: D,
    ctx: &'a mut DeserializerContext,
}

impl<'a, 'de, D> SerdeDeserializer<'de> for ClassDeserializer<'a, D>
where
    D: HaxeDeserializer<'de>,
{
    type Error = HaxeError;
    serde::forward_to_deserialize_any!(
        bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf unit option seq tuple
        tuple_struct map enum identifier ignored_any
    );

    fn deserialize_any<V: SerdeVisitor<'de>>(self, visitor: V) -> Result<V::Value> {
        self.de.deserialize_any(ClassVisitor {
            name: "<not a struct>",
            ctx: self.ctx,
            vis: visitor,
        })
    }

    fn deserialize_unit_struct<V: SerdeVisitor<'de>>(
        self,
        name: &'static str,
        visitor: V,
    ) -> Result<V::Value> {
        self.deserialize_struct(name, &[], visitor)
    }

    fn deserialize_struct<V: SerdeVisitor<'de>>(
        self,
        name: &'static str,
        _fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value> {
        self.de.deserialize_any(ClassVisitor {
            name,
            ctx: self.ctx,
            vis: visitor,
        })
    }

    fn deserialize_newtype_struct<V: SerdeVisitor<'de>>(
        self,
        name: &'static str,
        visitor: V,
    ) -> Result<V::Value> {
        match AdapterMarker::parse(name) {
            None => visitor.visit_newtype_struct(self),
            Some(adapter) => Err(HaxeError::unrepresentable(format!(
                "invalid nested adapter: {:?}",
                adapter
            ))),
        }
    }
}

macro_rules! impl_visitor {
    ($($kind:ident)* => $body:expr) => {
        fn visit_object_ref(self, _: ObjectRef) -> Result<Self::Value> {
            Err(HaxeError::invalid_input("can't deserialize object references"))
        }

        $(impl_visitor!(@$kind $body);)*
    };
    (@body $body:expr, $got:expr) => {{
        let got = $got;
        let body = $body;
        body(got)
    }};
    (@null $body:expr) => { fn visit_null(self) -> Result<Self::Value> {
        impl_visitor!(@body $body, "null")
    }};
    (@bool $body:expr) => { fn visit_bool(self, _: bool) -> Result<Self::Value> {
        impl_visitor!(@body $body, "bool")
    }};
    (@int $body:expr) => { fn visit_int(self, _: i64) -> Result<Self::Value> {
        impl_visitor!(@body $body, "int")
    }};
    (@float $body:expr) => { fn visit_float(self, _: f64) -> Result<Self::Value> {
        impl_visitor!(@body $body, "float")
    }};
    (@str $body:expr) => { fn visit_str(self, _: StringWithRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "string")
    }};
    (@bytes $body:expr) => { fn visit_bytes(self, _: Vec<u8>, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "bytes")
    }};
    (@date $body:expr) => { fn visit_date(self, _: HaxeDate, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "date")
    }};
    (@array $body:expr) => { fn visit_array<A: raw::SeqAccess<'de>>(self,
            _: A, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "array")
    }};
    (@list $body:expr) => { fn visit_list<A: raw::SeqAccess<'de>>(self,
            _: A, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "list")
    }};
    (@struct $body:expr) => { fn visit_struct<A: raw::StrMapAccess<'de>>(self,
        _: A, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "struct")
    }};
    (@string_map $body:expr) => { fn visit_string_map<A: raw::StrMapAccess<'de>>(self,
        _: A, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "string map")
    }};
    (@int_map $body:expr) => { fn visit_int_map<A: raw::IntMapAccess<'de>>(self,
        _: A, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "int map")
    }};
    (@object_map $body:expr) => { fn visit_object_map<A: raw::ObjMapAccess<'de>>(self,
        _: A, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "object map")
    }};
    (@class $body:expr) => { fn visit_class<A: raw::StrMapAccess<'de>>(self,
        _: StringWithRef, _: A, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "class")
    }};
    (@enum $body:expr) => {
        fn visit_enum_by_index<A: raw::SeqAccess<'de>>(self,
            _: StringWithRef, _: usize, _: A, _: ObjectRef) -> Result<Self::Value> {
            impl_visitor!(@body $body, "enum")
        }
        fn visit_enum_by_name<A: raw::SeqAccess<'de>>(self,
            _: StringWithRef, _: StringWithRef, _: A, _: ObjectRef) -> Result<Self::Value> {
            impl_visitor!(@body $body, "enum")
        }
    };
    (@class_def $body:expr) => { fn visit_class_def(self,
            _: StringWithRef, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "class def")
    }};
    (@enum_def $body:expr) => { fn visit_enum_def(self,
            _: StringWithRef, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "denum def")
    }};
    (@custom $body:expr) => { fn visit_custom<A: raw::SeqAccess<'de>>(self,
            _: StringWithRef, _: A, _: ObjectRef) -> Result<Self::Value> {
        impl_visitor!(@body $body, "custom class")
    }};
    (@exception $body:expr) => { fn visit_exception<D: HaxeDeserializer<'de>>(self,
            _: D) -> Result<Self::Value> {
        impl_visitor!(@body $body, "exception")
    }};
}

struct SimpleVisitor<'a, V, K> {
    vis: V,
    ctx: &'a mut DeserializerContext,
    _kind: PhantomData<K>,
}

trait VisitorKind {
    fn deserialize_with<'de, D: HaxeDeserializer<'de>, V: SerdeVisitor<'de>>(
        ctx: &mut DeserializerContext,
        de: D,
        vis: V,
    ) -> Result<V::Value>;
}

macro_rules! simple_visitor {
    (
        impl $kind:ty { $($impl_macro:tt)* }
        $($impl_vis:item)*
    ) => {
        impl VisitorKind for $kind {
            fn deserialize_with<'de, D: HaxeDeserializer<'de>, V: SerdeVisitor<'de>>(
                    ctx: &mut DeserializerContext, de: D, vis: V) -> Result<V::Value> {
                de.deserialize_any(SimpleVisitor { ctx, vis, _kind: PhantomData::<Self> })
            }
        }

        impl<'a, 'de, V> HaxeVisitor<'de> for SimpleVisitor<'a, V, $kind> where V: SerdeVisitor<'de> {
            type Value = V::Value;
            impl_visitor!($($impl_macro)*);
            $($impl_vis)*
        }
    }
}

simple_visitor! {
    impl () {
        date list string_map int_map object_map
        class enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("value", kind))
        }
    }

    fn visit_null(self) -> Result<Self::Value> {
        self.vis.visit_unit()
    }

    fn visit_bool(self, v: bool) -> Result<Self::Value> {
        self.vis.visit_bool(v)
    }

    fn visit_int(self, v: i64) -> Result<Self::Value> {
        self.vis.visit_i64(v)
    }

    fn visit_float(self, v: f64) -> Result<Self::Value> {
        self.vis.visit_f64(v)
    }

    fn visit_str(self, v: StringWithRef) -> Result<Self::Value> {
        let v = self.ctx.get_string(v)?;
        self.vis.visit_str(v)
    }

    fn visit_bytes(self, v: Vec<u8>, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_byte_buf(v)
    }

    fn visit_array<A: raw::SeqAccess<'de>>(self, seq: A, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_seq(SeqAccess { ctx: self.ctx, seq })
    }

    fn visit_struct<A: raw::StrMapAccess<'de>>(self, fields: A, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_map(StrMapAccess { ctx: self.ctx, map: fields })
    }
}

simple_visitor! {
    impl DateAdapter<()> {
        null bool int float str bytes array list string_map int_map object_map
        struct class enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("date", kind))
        }
    }

    fn visit_date(self, v: HaxeDate, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_i64(chrono::NaiveDateTime::from(v).timestamp())
    }
}

simple_visitor! {
    impl ArrayAdapter<()> {
        null bool int float str bytes date list string_map int_map object_map
        struct class enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("array", kind))
        }
    }

    fn visit_array<A: raw::SeqAccess<'de>>(self, seq: A, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_seq(SeqAccess { seq, ctx: self.ctx })
    }
}

simple_visitor! {
    impl ListAdapter<()> {
        null bool int float str bytes date array string_map int_map object_map
        struct class enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("array", kind))
        }
    }

    fn visit_list<A: raw::SeqAccess<'de>>(self, seq: A, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_seq(SeqAccess { seq, ctx: self.ctx })
    }
}

simple_visitor! {
    impl StructAdapter<()> {
        null bool int float str bytes date array list string_map int_map object_map
        class enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("struct", kind))
        }
    }

    fn visit_struct<A: raw::StrMapAccess<'de>>(self, map: A, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_map(StrMapAccess { map, ctx: self.ctx })
    }
}

simple_visitor! {
    impl StringMapAdapter<()> {
        null bool int float str bytes date array list int_map object_map
        struct class enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("string map", kind))
        }
    }

    fn visit_string_map<A: raw::StrMapAccess<'de>>(self, map: A, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_map(StrMapAccess { map, ctx: self.ctx })
    }
}

simple_visitor! {
    impl IntMapAdapter<()> {
        null bool int float str bytes date array list string_map object_map
        struct class enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("int map", kind))
        }
    }

    fn visit_int_map<A: raw::IntMapAccess<'de>>(self, map: A, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_map(IntMapAccess { map, ctx: self.ctx })
    }
}

simple_visitor! {
    impl ObjectMapAdapter<()> {
        null bool int float str bytes date array list string_map int_map
        struct class enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("int map", kind))
        }
    }

    fn visit_object_map<A: raw::ObjMapAccess<'de>>(self, map: A, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_map(ObjectMapAccess { map, ctx: self.ctx })
    }
}

simple_visitor! {
    impl ClassDefAdapter<()> {
        null bool int float str bytes date array list string_map int_map object_map
        struct class enum enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("class def", kind))
        }
    }

    fn visit_class_def(self, name: StringWithRef, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_str(self.ctx.get_string(name)?)
    }
}

simple_visitor! {
    impl EnumDefAdapter<()> {
        null bool int float str bytes date array list string_map int_map object_map
        struct class enum class_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("enum def", kind))
        }
    }

    fn visit_enum_def(self, name: StringWithRef, _: ObjectRef) -> Result<Self::Value> {
        self.vis.visit_str(self.ctx.get_string(name)?)
    }
}

simple_visitor! {
    impl ExceptionAdapter<()> {
        null bool int float str bytes date array list string_map int_map object_map
        struct class enum class_def enum_def custom => |kind| {
            Err(HaxeError::unexpected_input("exception", kind))
        }
    }

    fn visit_exception<D: HaxeDeserializer<'de>>(self, exception: D) -> Result<Self::Value> {
        <() as VisitorKind>::deserialize_with(self.ctx, exception, self.vis)
    }
}

struct ClassVisitor<'a, V> {
    ctx: &'a mut DeserializerContext,
    vis: V,
    name: &'a str,
}

impl<'a, 'de, V> HaxeVisitor<'de> for ClassVisitor<'a, V>
where
    V: SerdeVisitor<'de>,
{
    type Value = V::Value;
    impl_visitor! {
        null bool int float str bytes date array list string_map int_map object_map
        struct enum class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("class", kind))
        }
    }

    fn visit_class<A: raw::StrMapAccess<'de>>(
        self,
        name: StringWithRef,
        fields: A,
        _: ObjectRef,
    ) -> Result<Self::Value> {
        validate_name(self.ctx, self.name, name, "class")?;
        self.vis.visit_map(StrMapAccess {
            map: fields,
            ctx: self.ctx,
        })
    }
}

struct EnumVisitor<'a, V> {
    ctx: &'a mut DeserializerContext,
    vis: V,
    name: &'a str,
    fields: &'a [&'a str],
}

impl<'a, 'de, V> HaxeVisitor<'de> for EnumVisitor<'a, V>
where
    V: SerdeVisitor<'de>,
{
    type Value = V::Value;
    impl_visitor! {
        null bool int float str bytes date array list string_map int_map object_map
        struct class class_def enum_def custom exception => |kind| {
            Err(HaxeError::unexpected_input("enum", kind))
        }
    }

    fn visit_enum_by_index<A: raw::SeqAccess<'de>>(
        self,
        name: StringWithRef,
        variant_index: usize,
        data: A,
        _: ObjectRef,
    ) -> Result<Self::Value> {
        validate_name(self.ctx, self.name, name, "enum")?;
        let variant = self.fields.get(variant_index).ok_or_else(|| {
            HaxeError::invalid_input(format!(
                "invalid variant id {} for enum {}",
                variant_index, self.name
            ))
        })?;
        self.vis.visit_enum(EnumAccess {
            variant: StringArg::Str(variant),
            data,
            ctx: self.ctx,
        })
    }

    fn visit_enum_by_name<A: raw::SeqAccess<'de>>(
        self,
        name: StringWithRef,
        variant_name: StringWithRef,
        data: A,
        _: ObjectRef,
    ) -> Result<Self::Value> {
        validate_name(self.ctx, self.name, name, "enum")?;
        let (variant_str, variant) = variant_name;
        if let Some(s) = variant_str {
            self.ctx.cache.put_string(s, variant);
        }
        self.vis.visit_enum(EnumAccess {
            variant: StringArg::Ref(variant),
            data,
            ctx: self.ctx,
        })
    }
}

fn validate_name(
    ctx: &mut DeserializerContext,
    expected: &str,
    name: StringWithRef,
    kind: &str,
) -> Result<()> {
    let name = ctx.get_string(name)?;
    if name == expected {
        return Ok(());
    }
    Err(HaxeError::unexpected_input(
        format!("{} {}", kind, expected),
        format!("{} {}", kind, name),
    ))
}

struct EnumAccess<'a, A> {
    data: A,
    variant: StringArg<'a>,
    ctx: &'a mut DeserializerContext,
}

impl<'a, 'de, A> de::EnumAccess<'de> for EnumAccess<'a, A>
where
    A: raw::SeqAccess<'de>,
{
    type Error = HaxeError;
    type Variant = Self;
    fn variant_seed<V: SerdeDeserializeSeed<'de>>(
        self,
        seed: V,
    ) -> Result<(V::Value, Self::Variant)> {
        let variant = match self.variant {
            StringArg::Str(s) => s,
            StringArg::Ref(r) => self.ctx.get_string((None, r))?,
        };
        seed.deserialize(variant.into_deserializer())
            .map(|v| (v, self))
    }
}

impl<'a, 'de, A> de::VariantAccess<'de> for EnumAccess<'a, A>
where
    A: raw::SeqAccess<'de>,
{
    type Error = HaxeError;
    fn unit_variant(self) -> Result<()> {
        Ok(())
    }

    fn newtype_variant_seed<T: SerdeDeserializeSeed<'de>>(mut self, seed: T) -> Result<T::Value> {
        let value = self
            .data
            .next_element(self.ctx.with_seed(seed))?
            .ok_or_else(|| HaxeError::unexpected_input("newtype variant", "unit variant"))?;
        Ok(value)
    }

    fn tuple_variant<V: SerdeVisitor<'de>>(self, _len: usize, visitor: V) -> Result<V::Value> {
        visitor.visit_seq(SeqAccess {
            seq: self.data,
            ctx: self.ctx,
        })
    }

    fn struct_variant<V: SerdeVisitor<'de>>(
        self,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value> {
        struct StructVariantAccess<'a, A> {
            fields: &'static [&'static str],
            num: usize,
            seq: A,
            ctx: &'a mut DeserializerContext,
        }

        impl<'a, 'de, A> de::MapAccess<'de> for StructVariantAccess<'a, A>
        where
            A: raw::SeqAccess<'de>,
        {
            type Error = HaxeError;
            fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
            where
                K: SerdeDeserializeSeed<'de>,
            {
                self.fields
                    .get(self.num)
                    .map(|name| {
                        let de = serde::de::value::BorrowedStrDeserializer::new(name);
                        seed.deserialize(de)
                    })
                    .transpose()
            }

            fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
            where
                V: SerdeDeserializeSeed<'de>,
            {
                self.num += 1;
                let value = self.seq.next_element(self.ctx.with_seed(seed))?;
                Ok(value.expect("next_value_seed was called too many times"))
            }
        }

        visitor.visit_map(StructVariantAccess {
            fields,
            num: 0,
            seq: self.data,
            ctx: self.ctx,
        })
    }
}

struct SeqAccess<'a, A> {
    seq: A,
    ctx: &'a mut DeserializerContext,
}

impl<'a, 'de, A> de::SeqAccess<'de> for SeqAccess<'a, A>
where
    A: raw::SeqAccess<'de>,
{
    type Error = HaxeError;
    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: SerdeDeserializeSeed<'de>,
    {
        self.seq.next_element(self.ctx.with_seed(seed))
    }
}

struct StrMapAccess<'a, A> {
    map: A,
    ctx: &'a mut DeserializerContext,
}

impl<'a, 'de, A> de::MapAccess<'de> for StrMapAccess<'a, A>
where
    A: raw::StrMapAccess<'de>,
{
    type Error = HaxeError;
    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
    where
        K: SerdeDeserializeSeed<'de>,
    {
        match self.map.next_key()? {
            Some(key) => {
                let key = self.ctx.get_string(key)?;
                seed.deserialize(key.into_deserializer()).map(Some)
            }
            None => Ok(None),
        }
    }
    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
    where
        V: SerdeDeserializeSeed<'de>,
    {
        self.map.next_value(self.ctx.with_seed(seed))
    }
}

struct IntMapAccess<'a, A> {
    map: A,
    ctx: &'a mut DeserializerContext,
}

impl<'a, 'de, A> de::MapAccess<'de> for IntMapAccess<'a, A>
where
    A: raw::IntMapAccess<'de>,
{
    type Error = HaxeError;
    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
    where
        K: SerdeDeserializeSeed<'de>,
    {
        match self.map.next_key()? {
            Some(key) => seed.deserialize(key.into_deserializer()).map(Some),
            None => Ok(None),
        }
    }
    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
    where
        V: SerdeDeserializeSeed<'de>,
    {
        self.map.next_value(self.ctx.with_seed(seed))
    }
}

struct ObjectMapAccess<'a, A> {
    map: A,
    ctx: &'a mut DeserializerContext,
}

impl<'a, 'de, A> de::MapAccess<'de> for ObjectMapAccess<'a, A>
where
    A: raw::ObjMapAccess<'de>,
{
    type Error = HaxeError;
    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
    where
        K: SerdeDeserializeSeed<'de>,
    {
        self.map.next_key(self.ctx.with_seed(seed))
    }
    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
    where
        V: SerdeDeserializeSeed<'de>,
    {
        self.map.next_value(self.ctx.with_seed(seed))
    }
}
