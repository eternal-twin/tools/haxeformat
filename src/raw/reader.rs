use std::io::{self, Read};

use super::chars;
use super::de::{self, HaxeDeserializer, HaxeVisitor, StringWithRef};
use super::error::ErrorKind;
use super::{HaxeDate, ObjectRef, Result, StringRef};

use self::flavors::*;
use self::sub_deserializers::*;

pub struct ReadDeserializer<R> {
    reader: R,
    peek: Option<u8>,
    tmp_buf: Vec<u8>,
    next_string_ref: StringRef,
    next_obj_ref: ObjectRef,
}

impl<R: Read> ReadDeserializer<R> {
    pub fn new(reader: R) -> Self {
        ReadDeserializer {
            reader,
            peek: None,
            tmp_buf: Vec::new(),
            next_string_ref: StringRef(0),
            next_obj_ref: ObjectRef(0),
        }
    }

    pub fn has_next(&mut self) -> bool {
        matches!(self.try_peek_byte(), Ok(Some(_)))
    }

    pub fn end(&mut self) -> Result<()> {
        match self.try_peek_byte() {
            Ok(Some(_)) => Err(ErrorKind::TrailingBytes.into()),
            Ok(None) => Ok(()),
            Err(err) => Err(err),
        }
    }

    fn read_byte(&mut self) -> Result<u8> {
        self.peek_byte()?;
        Ok(self.peek.take().unwrap())
    }

    fn try_peek_byte(&mut self) -> Result<Option<u8>> {
        let mut byte = 0;
        match self.peek {
            Some(byte) => Ok(Some(byte)),
            None => loop {
                return match self.reader.read(std::slice::from_mut(&mut byte)) {
                    Ok(0) => Ok(None),
                    Ok(_) => {
                        self.peek = Some(byte);
                        Ok(self.peek)
                    }
                    Err(err) if err.kind() == io::ErrorKind::Interrupted => continue,
                    Err(err) => Err(err.into()),
                };
            },
        }
    }

    fn peek_byte(&mut self) -> Result<u8> {
        self.try_peek_byte()?.ok_or_else(|| {
            io::Error::new(io::ErrorKind::UnexpectedEof, "failed to read one byte").into()
        })
    }

    fn discard(&mut self) {
        self.peek = None;
    }

    fn expect_byte(&mut self, expected: u8) -> Result<()> {
        let b = self.read_byte()?;
        if b == expected {
            Ok(())
        } else {
            Err(ErrorKind::InvalidByte(b).into())
        }
    }

    fn read_buf(&mut self, mut buf: &mut [u8]) -> Result<()> {
        if buf.is_empty() {
            Ok(())
        } else {
            if let Some(b) = self.peek.take() {
                buf[0] = b;
                buf = &mut buf[1..];
            }
            Ok(self.reader.read_exact(buf)?)
        }
    }

    fn read_while(&mut self, mut f: impl FnMut(u8) -> bool) -> Result<&[u8]> {
        self.tmp_buf.clear();
        while let Some(byte) = self.try_peek_byte()? {
            if f(byte) {
                self.tmp_buf.push(byte);
                self.discard();
            } else {
                break;
            }
        }
        Ok(&self.tmp_buf)
    }

    fn read_digits(&mut self) -> Result<&str> {
        self.read_while(|b| matches!(b, b'0'..=b'9' | b'-' | b'+' | b'.' | b'e' | b'E'))
            .map(|bytes| unsafe {
                // Safe, because we only read ASCII bytes
                std::str::from_utf8_unchecked(bytes)
            })
    }

    fn read_len(&mut self) -> Result<usize> {
        self.read_digits()?
            .parse()
            .map_err(|_| ErrorKind::InvalidNumber.into())
    }

    fn read_int(&mut self) -> Result<i64> {
        self.read_digits()?
            .parse()
            .map_err(|_| ErrorKind::InvalidNumber.into())
    }

    fn read_float(&mut self) -> Result<f64> {
        self.read_digits()?
            .parse()
            .map_err(|_| ErrorKind::InvalidNumber.into())
    }

    fn read_string_with_first(&mut self, first: u8) -> Result<StringWithRef> {
        match first {
            chars::String => {
                let new_ref = self.next_string_ref;
                self.next_string_ref.0 += 1;
                let len = self.read_len()?;
                self.expect_byte(chars::Separator)?;
                let string = url_decode(self, len)?;
                Ok((Some(string), new_ref))
            }
            chars::StringRef => Ok((None, StringRef(self.read_len()?))),
            byte => Err(ErrorKind::InvalidByte(byte).into()),
        }
    }

    fn read_string(&mut self) -> Result<StringWithRef> {
        let first = self.read_byte()?;
        self.read_string_with_first(first)
    }

    fn read_date(&mut self) -> Result<HaxeDate> {
        let mut is_date = false;
        let date_or_timestamp = self.read_while(|b| match b {
            b'0'..=b'9' | b'-' | b'+' => true,
            b' ' | b':' => {
                is_date = true;
                true
            }
            _ => false,
        })?;
        // Safe, because we only read ASCII bytes
        let date_or_timestamp = unsafe { std::str::from_utf8_unchecked(date_or_timestamp) };

        let date = if is_date {
            chrono::NaiveDateTime::parse_from_str(date_or_timestamp, chars::DATE_FORMAT)
                .ok()
                .and_then(HaxeDate::try_new)
        } else {
            date_or_timestamp
                .parse()
                .ok()
                .and_then(HaxeDate::try_from_timestamp)
        };

        date.ok_or_else(|| ErrorKind::InvalidDate.into())
    }

    fn create_obj_ref(&mut self) -> ObjectRef {
        let new_ref = self.next_obj_ref;
        self.next_obj_ref.0 += 1;
        new_ref
    }

    fn assert_seq_end<T, L, F>(&mut self, mut flavor: L, f: F) -> Result<T>
    where
        L: SeqLenFlavor,
        F: FnOnce(&mut Self, &mut L) -> Result<T>,
    {
        let res = f(self, &mut flavor)?;
        if flavor.has_next_element(self)? {
            Err(ErrorKind::SeqTrailingValues.into())
        } else {
            Ok(res)
        }
    }
}

impl<'a, 'de, R: Read> HaxeDeserializer<'de> for &'a mut ReadDeserializer<R> {
    fn peek_null(&mut self) -> Result<bool> {
        Ok(self.try_peek_byte()? == Some(chars::Null))
    }

    fn deserialize_any<V: HaxeVisitor<'de>>(self, visitor: V) -> Result<V::Value> {
        match self.read_byte()? {
            chars::Null => visitor.visit_null(),
            chars::True => visitor.visit_bool(true),
            chars::False => visitor.visit_bool(false),
            chars::Zero => visitor.visit_int(0),
            chars::NonZeroInt => visitor.visit_int(self.read_int()?),
            chars::NaN => visitor.visit_float(f64::NAN),
            chars::PositiveInf => visitor.visit_float(f64::INFINITY),
            chars::NegativeInf => visitor.visit_float(f64::NEG_INFINITY),
            chars::FiniteFloat => visitor.visit_float(self.read_float()?),
            chars::ObjectRef => visitor.visit_object_ref(ObjectRef(self.read_len()?)),
            chars::Bytes => {
                let len = self.read_len()?;
                self.expect_byte(chars::Separator)?;
                visitor.visit_bytes(base64_read(self, len)?, self.create_obj_ref())
            }
            chars::Date => visitor.visit_date(self.read_date()?, self.create_obj_ref()),
            chars::Array => {
                let obj = self.create_obj_ref();
                self.assert_seq_end(ByteTerminated(chars::SeqEnd), |this, len| {
                    visitor.visit_array(SeqReader::new(this, CoalesceNullsFlavor(0), len), obj)
                })
            }
            chars::List => {
                let obj = self.create_obj_ref();
                self.assert_seq_end(ByteTerminated(chars::SeqEnd), |this, len| {
                    visitor.visit_list(SeqReader::new(this, (), len), obj)
                })
            }
            chars::Struct => {
                let obj = self.create_obj_ref();
                self.assert_seq_end(ByteTerminated(chars::ObjEnd), |this, len| {
                    visitor.visit_struct(MapReader::new(this, len), obj)
                })
            }
            chars::StringMap => {
                let obj = self.create_obj_ref();
                self.assert_seq_end(ByteTerminated(chars::SeqEnd), |this, len| {
                    visitor.visit_string_map(MapReader::new(this, len), obj)
                })
            }
            chars::IntMap => {
                let obj = self.create_obj_ref();
                self.assert_seq_end(ByteTerminated(chars::SeqEnd), |this, len| {
                    visitor.visit_int_map(MapReader::new(this, len), obj)
                })
            }
            chars::ObjectMap => {
                let obj = self.create_obj_ref();
                self.assert_seq_end(ByteTerminated(chars::SeqEnd), |this, len| {
                    visitor.visit_object_map(MapReader::new(this, len), obj)
                })
            }
            chars::Class => {
                let name = self.read_string()?;
                let obj = self.create_obj_ref();
                self.assert_seq_end(ByteTerminated(chars::ObjEnd), |this, len| {
                    visitor.visit_class(name, MapReader::new(this, len), obj)
                })
            }
            chars::EnumByIndex => {
                let name = self.read_string()?;
                self.expect_byte(chars::Separator)?;
                let variant = self.read_len()?;
                self.expect_byte(chars::Separator)?;
                let len = self.read_len()?;
                let obj = self.create_obj_ref();
                self.assert_seq_end(KnownLength(len), |this, len| {
                    visitor.visit_enum_by_index(name, variant, SeqReader::new(this, (), len), obj)
                })
            }
            chars::EnumByName => {
                let name = self.read_string()?;
                let variant = self.read_string()?;
                self.expect_byte(chars::Separator)?;
                let len = self.read_len()?;
                let obj = self.create_obj_ref();
                self.assert_seq_end(KnownLength(len), |this, len| {
                    visitor.visit_enum_by_name(name, variant, SeqReader::new(this, (), len), obj)
                })
            }
            chars::ClassDef => visitor.visit_class_def(self.read_string()?, self.create_obj_ref()),
            chars::EnumDef => visitor.visit_enum_def(self.read_string()?, self.create_obj_ref()),
            chars::Custom => {
                let name = self.read_string()?;
                let obj = self.create_obj_ref();
                self.assert_seq_end(ByteTerminated(chars::ObjEnd), |this, len| {
                    visitor.visit_custom(name, SeqReader::new(this, (), len), obj)
                })
            }
            chars::Exception => visitor.visit_exception(self),
            other => visitor.visit_str(self.read_string_with_first(other)?),
        }
    }
}

mod flavors {
    use super::*;

    pub trait NullFlavor {
        fn try_read_null<R: Read>(&mut self, ctx: &mut ReadDeserializer<R>) -> Result<Option<()>>;
    }

    impl NullFlavor for () {
        fn try_read_null<R: Read>(&mut self, _ctx: &mut ReadDeserializer<R>) -> Result<Option<()>> {
            Ok(None)
        }
    }

    pub struct CoalesceNullsFlavor(pub(super) usize);

    impl NullFlavor for CoalesceNullsFlavor {
        fn try_read_null<R: Read>(&mut self, ctx: &mut ReadDeserializer<R>) -> Result<Option<()>> {
            if self.0 > 0 {
                self.0 -= 1;
                return Ok(Some(()));
            }
            if ctx.peek_byte()? == chars::ArrayNull {
                ctx.discard();
                let len = ctx.read_len()?;
                if len > 0 {
                    self.0 = len - 1;
                    return Ok(Some(()));
                }
            }
            Ok(None)
        }
    }

    pub trait SeqLenFlavor {
        fn has_next_element<R: Read>(&mut self, ctx: &mut ReadDeserializer<R>) -> Result<bool>;
        fn size_hint(&self) -> Option<usize>;
    }

    pub struct ByteTerminated(pub(super) u8);

    impl SeqLenFlavor for ByteTerminated {
        fn has_next_element<R: Read>(&mut self, ctx: &mut ReadDeserializer<R>) -> Result<bool> {
            match self.0 {
                0 => Ok(false),
                b if ctx.peek_byte()? == b => {
                        ctx.discard();
                        self.0 = 0;
                        Ok(false)
                    }

                    _ => Ok(true)
            }
        }

        fn size_hint(&self) -> Option<usize> {
            None
        }
    }

    pub struct KnownLength(pub(super) usize);

    impl SeqLenFlavor for KnownLength {
        fn has_next_element<R: Read>(&mut self, _ctx: &mut ReadDeserializer<R>) -> Result<bool> {
            if self.0 == 0 {
                Ok(false)
            } else {
                self.0 -= 1;
                Ok(true)
            }
        }

        fn size_hint(&self) -> Option<usize> {
            Some(self.0)
        }
    }
}

mod sub_deserializers {
    use super::*;

    struct NullDeserializer;
    impl<'de> HaxeDeserializer<'de> for NullDeserializer {
        fn deserialize_any<V: HaxeVisitor<'de>>(self, visitor: V) -> Result<V::Value> {
            visitor.visit_null()
        }

        fn peek_null(&mut self) -> Result<bool> {
            Ok(true)
        }
    }

    pub struct SeqReader<'a, R, N, L> {
        ctx: &'a mut ReadDeserializer<R>,
        nulls_flavor: N,
        len_flavor: &'a mut L,
    }

    impl<'a, R: Read, N: NullFlavor, L: SeqLenFlavor> SeqReader<'a, R, N, L> {
        pub(super) fn new(
            ctx: &'a mut ReadDeserializer<R>,
            nulls_flavor: N,
            len_flavor: &'a mut L,
        ) -> Self {
            SeqReader {
                ctx,
                nulls_flavor,
                len_flavor,
            }
        }
    }

    impl<'a, 'de, R: Read, N: NullFlavor, L: SeqLenFlavor> de::SeqAccess<'de>
        for SeqReader<'a, R, N, L>
    {
        fn next_element<S, T: de::HaxeDeserialize<'de, S>>(
            &mut self,
            state: S,
        ) -> Result<Option<T>> {
            if self.len_flavor.has_next_element(self.ctx)? {
                match self.nulls_flavor.try_read_null(self.ctx)? {
                    Some(()) => T::deserialize(state, NullDeserializer),
                    None => T::deserialize(state, &mut *self.ctx),
                }
                .map(Some)
            } else {
                Ok(None)
            }
        }

        fn size_hint(&mut self) -> Option<usize> {
            self.len_flavor.size_hint()
        }
    }

    pub struct MapReader<'a, R, L> {
        ctx: &'a mut ReadDeserializer<R>,
        len_flavor: &'a mut L,
    }

    impl<'a, R: Read, L: SeqLenFlavor> MapReader<'a, R, L> {
        pub fn new(ctx: &'a mut ReadDeserializer<R>, len_flavor: &'a mut L) -> Self {
            MapReader { ctx, len_flavor }
        }
    }

    impl<'a, 'de, R: Read, L: SeqLenFlavor> de::MapAccess<'de> for MapReader<'a, R, L> {
        fn next_value<S, T: de::HaxeDeserialize<'de, S>>(&mut self, state: S) -> Result<T> {
            T::deserialize(state, &mut *self.ctx)
        }

        fn size_hint(&mut self) -> Option<usize> {
            None
        }
    }

    impl<'a, 'de, R: Read, L: SeqLenFlavor> de::StrMapAccess<'de> for MapReader<'a, R, L> {
        fn next_key(&mut self) -> Result<Option<StringWithRef>> {
            if self.len_flavor.has_next_element(self.ctx)? {
                self.ctx.read_string().map(Some)
            } else {
                Ok(None)
            }
        }
    }

    impl<'a, 'de, R: Read, L: SeqLenFlavor> de::IntMapAccess<'de> for MapReader<'a, R, L> {
        fn next_key(&mut self) -> Result<Option<i64>> {
            if self.len_flavor.has_next_element(self.ctx)? {
                self.ctx.expect_byte(chars::Separator)?;
                self.ctx.read_int().map(Some)
            } else {
                Ok(None)
            }
        }
    }

    impl<'a, 'de, R: Read, L: SeqLenFlavor> de::ObjMapAccess<'de> for MapReader<'a, R, L> {
        fn next_key<S, T: de::HaxeDeserialize<'de, S>>(&mut self, state: S) -> Result<Option<T>> {
            if self.len_flavor.has_next_element(self.ctx)? {
                T::deserialize(state, &mut *self.ctx).map(Some)
            } else {
                Ok(None)
            }
        }
    }
}

fn url_decode<R: Read>(reader: &mut ReadDeserializer<R>, mut len: usize) -> Result<String> {
    let mut buf = Vec::with_capacity(len);
    while len > 0 {
        len -= 1;
        match reader.read_byte()? {
            b'%' => {
                len = len.checked_sub(2).ok_or(ErrorKind::InvalidString)?;
                let byte = chars::from_hex_digits(reader.read_byte()?, reader.read_byte()?)
                    .ok_or(ErrorKind::InvalidString)?;
                buf.push(byte);
            }
            byte => buf.push(chars::url_decode(byte).ok_or(ErrorKind::InvalidString)?),
        }
    }

    String::from_utf8(buf).map_err(|_| ErrorKind::InvalidString.into())
}

fn base64_read<R: Read>(reader: &mut ReadDeserializer<R>, mut len: usize) -> Result<Vec<u8>> {
    fn byte(b: u8) -> Result<u8> {
        chars::base64_decode(b).ok_or_else(|| ErrorKind::InvalidBase64.into())
    }
    let mut buf = Vec::with_capacity((len / 4) * 3 + (len % 4).saturating_sub(1));

    while len >= 4 {
        len -= 4;
        let mut tmp = [0; 4];
        reader.read_buf(&mut tmp)?;
        let (b1, b2, b3, b4) = (byte(tmp[0])?, byte(tmp[1])?, byte(tmp[2])?, byte(tmp[3])?);
        buf.extend_from_slice(&[(b1 << 2) | (b2 >> 4), (b2 << 4) | (b3 >> 2), (b3 << 6) | b4]);
    }

    match len {
        0 => (),
        2 => {
            let mut tmp = [0; 2];
            reader.read_buf(&mut tmp)?;
            let (b1, b2) = (byte(tmp[0])?, byte(tmp[1])?);
            buf.push((b1 << 2) | (b2 >> 4));
        }
        3 => {
            let mut tmp = [0; 3];
            reader.read_buf(&mut tmp)?;
            let (b1, b2, b3) = (byte(tmp[0])?, byte(tmp[1])?, byte(tmp[2])?);
            buf.extend_from_slice(&[(b1 << 2) | (b2 >> 4), (b2 << 4) | (b3 >> 2)]);
        }
        _ => return Err(ErrorKind::InvalidBase64.into()),
    }

    Ok(buf)
}
