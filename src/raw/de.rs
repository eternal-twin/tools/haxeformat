use super::{HaxeDate, HaxeError, ObjectRef, StringRef};

pub type StringWithRef = (Option<String>, StringRef);

pub trait HaxeDeserialize<'de, State>: Sized {
    fn deserialize<D: HaxeDeserializer<'de>>(
        state: State,
        deserializer: D,
    ) -> Result<Self, HaxeError>;
}

pub trait HaxeDeserializer<'de> {
    fn deserialize_any<V: HaxeVisitor<'de>>(self, visitor: V) -> Result<V::Value, HaxeError>;

    // Needed for serde compatibility
    fn peek_null(&mut self) -> Result<bool, HaxeError>;
}

pub trait HaxeVisitor<'de> {
    type Value;

    fn visit_null(self) -> Result<Self::Value, HaxeError>;
    fn visit_bool(self, v: bool) -> Result<Self::Value, HaxeError>;
    fn visit_int(self, v: i64) -> Result<Self::Value, HaxeError>;
    fn visit_float(self, v: f64) -> Result<Self::Value, HaxeError>;
    fn visit_object_ref(self, v: ObjectRef) -> Result<Self::Value, HaxeError>;
    fn visit_str(self, v: StringWithRef) -> Result<Self::Value, HaxeError>;
    fn visit_bytes(self, v: Vec<u8>, obj: ObjectRef) -> Result<Self::Value, HaxeError>;
    fn visit_date(self, v: HaxeDate, obj: ObjectRef) -> Result<Self::Value, HaxeError>;
    fn visit_array<A: SeqAccess<'de>>(
        self,
        seq: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_list<A: SeqAccess<'de>>(
        self,
        seq: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_struct<A: StrMapAccess<'de>>(
        self,
        fields: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_string_map<A: StrMapAccess<'de>>(
        self,
        map: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_int_map<A: IntMapAccess<'de>>(
        self,
        map: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_object_map<A: ObjMapAccess<'de>>(
        self,
        map: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_class<A: StrMapAccess<'de>>(
        self,
        name: StringWithRef,
        data: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_enum_by_index<A: SeqAccess<'de>>(
        self,
        name: StringWithRef,
        variant_index: usize,
        data: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_enum_by_name<A: SeqAccess<'de>>(
        self,
        name: StringWithRef,
        variant_name: StringWithRef,
        data: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_class_def(self, name: StringWithRef, obj: ObjectRef)
        -> Result<Self::Value, HaxeError>;
    fn visit_enum_def(self, name: StringWithRef, obj: ObjectRef) -> Result<Self::Value, HaxeError>;
    fn visit_custom<A: SeqAccess<'de>>(
        self,
        name: StringWithRef,
        data: A,
        obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError>;
    fn visit_exception<D: HaxeDeserializer<'de>>(
        self,
        exception: D,
    ) -> Result<Self::Value, HaxeError>;
}

pub trait SeqAccess<'de> {
    fn next_element<S, T: HaxeDeserialize<'de, S>>(
        &mut self,
        state: S,
    ) -> Result<Option<T>, HaxeError>;
    fn size_hint(&mut self) -> Option<usize>;
}

pub trait MapAccess<'de> {
    fn next_value<S, T: HaxeDeserialize<'de, S>>(&mut self, state: S) -> Result<T, HaxeError>;
    fn size_hint(&mut self) -> Option<usize>;
}

pub trait StrMapAccess<'de>: MapAccess<'de> {
    fn next_key(&mut self) -> Result<Option<StringWithRef>, HaxeError>;
}

pub trait IntMapAccess<'de>: MapAccess<'de> {
    fn next_key(&mut self) -> Result<Option<i64>, HaxeError>;
}

pub trait ObjMapAccess<'de>: MapAccess<'de> {
    fn next_key<S, T: HaxeDeserialize<'de, S>>(&mut self, state: S)
        -> Result<Option<T>, HaxeError>;
}
