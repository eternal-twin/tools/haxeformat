use std::io::{self, Write};
use std::num::FpCategory;

use super::ser::{self, HaxeSerialize, HaxeSerializer, StringArg};
use super::{chars, HaxeDate, ObjectRef, Result, StringRef};

use self::sub_serializers::*;

pub struct WriteSerializer<W> {
    writer: W,
    tmp_buf: Vec<u8>,
    pending_nulls: Option<usize>,
    next_string_ref: StringRef,
    next_obj_ref: ObjectRef,
}

impl<W: Write> WriteSerializer<W> {
    pub fn new(writer: W) -> Self {
        WriteSerializer {
            writer,
            tmp_buf: Vec::new(),
            pending_nulls: None,
            next_string_ref: StringRef(0),
            next_obj_ref: ObjectRef(0),
        }
    }

    pub fn into_inner(self) -> W {
        self.writer
    }

    fn write_byte(&mut self, byte: u8) -> Result<()> {
        Ok(self.writer.write_all(std::slice::from_ref(&byte))?)
    }

    fn write_len(&mut self, len: usize) -> Result<()> {
        let mut buf = itoa::Buffer::new();
        let bytes = buf.format(len).as_bytes();
        Ok(self.writer.write_all(bytes)?)
    }

    fn write_int(&mut self, v: i64) -> Result<()> {
        let mut buf = itoa::Buffer::new();
        let bytes = buf.format(v).as_bytes();
        Ok(self.writer.write_all(bytes)?)
    }

    fn write_float(&mut self, v: f64) -> Result<()> {
        let mut buf = dtoa::Buffer::new();
        let bytes = buf.format(v).as_bytes();
        Ok(self.writer.write_all(bytes)?)
    }

    fn write_string(&mut self, v: StringArg<'_>) -> Result<StringRef> {
        match v {
            StringArg::Str(string) => {
                let str_ref = self.next_string_ref;
                self.next_string_ref.0 += 1;
                self.tmp_buf.clear();
                url_encode(&mut self.tmp_buf, string);
                self.write_byte(chars::String)?;
                self.write_len(self.tmp_buf.len())?;
                self.write_byte(chars::Separator)?;
                self.writer.write_all(&self.tmp_buf)?;
                Ok(str_ref)
            }
            StringArg::Ref(str_ref) => {
                self.write_byte(chars::StringRef)?;
                self.write_len(str_ref.0)?;
                Ok(str_ref)
            }
        }
    }

    fn write_null(&mut self) -> Result<()> {
        if let Some(nulls) = self.pending_nulls.as_mut() {
            *nulls += 1;
            return Ok(());
        }
        self.write_byte(chars::Null)
    }

    fn flush_nulls(&mut self) -> Result<()> {
        let nulls = match self.pending_nulls {
            Some(n) => n,
            None => return Ok(()),
        };
        match nulls {
            0 => (),
            1 => self.write_byte(chars::Null)?,
            nulls => {
                self.write_byte(chars::ArrayNull)?;
                self.write_len(nulls)?;
            }
        }
        self.pending_nulls = Some(0);
        Ok(())
    }

    fn create_object_ref(&mut self) -> ObjectRef {
        let obj_ref = self.next_obj_ref;
        self.next_obj_ref.0 += 1;
        obj_ref
    }
}

impl<'a, W: Write> HaxeSerializer for &'a mut WriteSerializer<W> {
    type Ok = ();
    type String = SimpleString;
    type Bytes = SimpleObject;
    type Date = SimpleObject;
    type Array = Compound<'a, W>;
    type List = Compound<'a, W>;
    type Struct = Compound<'a, W>;
    type StringMap = Compound<'a, W>;
    type IntMap = Compound<'a, W>;
    type ObjectMap = Compound<'a, W>;
    type Class = Compound<'a, W>;
    type EnumByIndex = Compound<'a, W>;
    type EnumByName = Compound<'a, W>;
    type ClassDef = SimpleString;
    type EnumDef = SimpleString;
    type Custom = Compound<'a, W>;

    fn serialize_null(self) -> Result<()> {
        self.write_null()
    }

    fn serialize_bool(self, v: bool) -> Result<()> {
        self.flush_nulls()?;
        self.write_byte(if v { chars::True } else { chars::False })
    }

    fn serialize_int(self, v: i64) -> Result<()> {
        self.flush_nulls()?;
        if v == 0 {
            self.write_byte(chars::Zero)
        } else {
            self.write_byte(chars::NonZeroInt)?;
            self.write_int(v)
        }
    }

    fn serialize_float(self, v: f64) -> Result<()> {
        self.flush_nulls()?;
        let prefix = match v.classify() {
            FpCategory::Nan => chars::NaN,
            FpCategory::Infinite if v.is_sign_negative() => chars::NegativeInf,
            FpCategory::Infinite => chars::PositiveInf,
            _ => chars::FiniteFloat,
        };
        self.write_byte(prefix)?;
        if prefix == chars::FiniteFloat {
            self.write_float(v)?;
        }
        Ok(())
    }

    fn serialize_string(self, v: StringArg<'_>) -> Result<SimpleString> {
        self.flush_nulls()?;
        self.write_string(v).map(SimpleString)
    }

    fn serialize_bytes(self, bytes: &[u8]) -> Result<SimpleObject> {
        self.flush_nulls()?;
        let obj_ref = self.create_object_ref();
        self.write_byte(chars::Bytes)?;
        self.write_len(base64_len(bytes))?;
        self.write_byte(chars::Separator)?;
        write_base64(&mut self.writer, bytes)?;
        Ok(SimpleObject(obj_ref))
    }

    fn serialize_date(self, date: HaxeDate) -> Result<SimpleObject> {
        self.flush_nulls()?;
        let obj_ref = self.create_object_ref();
        self.write_byte(chars::Date)?;
        // TODO: add support for serializing as timestamp (with a config switch?)
        let date = chrono::NaiveDateTime::from(date);
        write!(self.writer, "{}", date.format(chars::DATE_FORMAT))?;
        Ok(SimpleObject(obj_ref))
    }

    fn serialize_object_ref(self, obj_ref: ObjectRef) -> Result<()> {
        self.flush_nulls()?;
        self.write_byte(chars::ObjectRef)?;
        self.write_len(obj_ref.0)
    }

    fn serialize_array(self) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::Array)?;
        Ok(Compound::new_coalescing_nulls(self, chars::SeqEnd))
    }

    fn serialize_list(self) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::List)?;
        Ok(Compound::new(self, chars::SeqEnd))
    }

    fn serialize_struct(self) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::Struct)?;
        Ok(Compound::new(self, chars::ObjEnd))
    }

    fn serialize_string_map(self) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::StringMap)?;
        Ok(Compound::new(self, chars::SeqEnd))
    }

    fn serialize_int_map(self) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::IntMap)?;
        Ok(Compound::new(self, chars::SeqEnd))
    }

    fn serialize_object_map(self) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::ObjectMap)?;
        Ok(Compound::new(self, chars::SeqEnd))
    }

    fn serialize_class(self, name: StringArg<'_>) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::Class)?;
        let name = self.write_string(name)?;
        Ok(Compound::new_named(self, chars::ObjEnd, name, None))
    }

    fn serialize_enum_by_index(
        self,
        name: StringArg<'_>,
        variant_index: usize,
        nb_params: usize,
    ) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::EnumByIndex)?;
        let name = self.write_string(name)?;
        self.write_byte(chars::Separator)?;
        self.write_len(variant_index)?;
        self.write_byte(chars::Separator)?;
        self.write_len(nb_params)?;
        Ok(Compound::new_named(self, 0, name, None))
    }

    fn serialize_enum_by_name(
        self,
        name: StringArg<'_>,
        variant_name: StringArg<'_>,
        nb_params: usize,
    ) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::EnumByName)?;
        let name = self.write_string(name)?;
        let variant = self.write_string(variant_name)?;
        self.write_byte(chars::Separator)?;
        self.write_len(nb_params)?;
        Ok(Compound::new_named(self, 0, name, Some(variant)))
    }

    fn serialize_class_def(self, name: StringArg<'_>) -> Result<SimpleString> {
        self.flush_nulls()?;
        self.write_byte(chars::ClassDef)?;
        self.write_string(name).map(SimpleString)
    }

    fn serialize_enum_def(self, name: StringArg<'_>) -> Result<SimpleString> {
        self.flush_nulls()?;
        self.write_byte(chars::EnumDef)?;
        self.write_string(name).map(SimpleString)
    }

    fn serialize_exception<'ser, S, E: HaxeSerialize<'ser, S> + ?Sized>(
        self,
        state: S,
        exception: &'ser E,
    ) -> Result<()> {
        self.flush_nulls()?;
        self.write_byte(chars::Exception)?;
        exception.serialize(state, self)
    }

    fn serialize_custom(self, name: StringArg<'_>) -> Result<Compound<'a, W>> {
        self.flush_nulls()?;
        self.write_byte(chars::Custom)?;
        let name = self.write_string(name)?;
        Ok(Compound::new_named(self, chars::ObjEnd, name, None))
    }
}

mod sub_serializers {
    use super::*;

    pub struct SimpleString(pub(super) StringRef);

    impl ser::StringSer for SimpleString {
        type Ok = ();
        fn string_ref(&mut self) -> StringRef {
            self.0
        }
        fn end(self) -> Result<()> {
            Ok(())
        }
    }

    pub struct SimpleObject(pub(super) ObjectRef);

    impl ser::ObjectSer for SimpleObject {
        type Ok = ();
        fn object_ref(&mut self) -> ObjectRef {
            self.0
        }
        fn end(self) -> Result<()> {
            Ok(())
        }
    }

    pub struct Compound<'a, W> {
        object_ref: ObjectRef,
        previous_null_state: Option<usize>,
        ctx: &'a mut WriteSerializer<W>,
        end_byte: u8,            // if zero, no byte to write
        name: StringRef,         // dummy value if unused
        variant_name: StringRef, // dummy value if unused
    }

    impl<'a, W: Write> Compound<'a, W> {
        pub(super) fn new(ctx: &'a mut WriteSerializer<W>, end_byte: u8) -> Self {
            let mut result = Compound {
                object_ref: ctx.create_object_ref(),
                previous_null_state: ctx.pending_nulls,
                ctx,
                end_byte,
                name: StringRef(usize::max_value()),
                variant_name: StringRef(usize::max_value()),
            };
            result.ctx.pending_nulls = None;
            result
        }

        pub(super) fn new_coalescing_nulls(ctx: &'a mut WriteSerializer<W>, end_byte: u8) -> Self {
            let mut result = Self::new(ctx, end_byte);
            result.ctx.pending_nulls = Some(0);
            result
        }

        pub(super) fn new_named(
            ctx: &'a mut WriteSerializer<W>,
            end_byte: u8,
            name: StringRef,
            variant_name: Option<StringRef>,
        ) -> Self {
            let mut result = Self::new(ctx, end_byte);
            result.name = name;
            result.variant_name = variant_name.unwrap_or(StringRef(usize::max_value()));
            result
        }
    }

    impl<'a, W: Write> ser::ObjectSer for Compound<'a, W> {
        type Ok = ();

        fn object_ref(&mut self) -> ObjectRef {
            self.object_ref
        }

        fn end(mut self) -> Result<()> {
            self.ctx.flush_nulls()?;
            self.ctx.pending_nulls = self.previous_null_state;
            if self.end_byte != 0 {
                self.ctx.write_byte(self.end_byte)
            } else {
                Ok(())
            }
        }
    }

    impl<'a, W: Write> ser::SeqSer for Compound<'a, W> {
        fn serialize_elem<'ser, S, E: HaxeSerialize<'ser, S> + ?Sized>(
            &mut self,
            state: S,
            elem: &'ser E,
        ) -> Result<()> {
            elem.serialize(state, &mut *self.ctx)
        }
    }

    impl<'a, W: Write> ser::MapSer for Compound<'a, W> {
        fn serialize_value<'ser, S, V: HaxeSerialize<'ser, S> + ?Sized>(
            &mut self,
            state: S,
            value: &'ser V,
        ) -> Result<()> {
            value.serialize(state, &mut *self.ctx)
        }
    }

    impl<'a, W: Write> ser::StringMapSer for Compound<'a, W> {
        type MapStringKey = SimpleString;
        fn serialize_key(&mut self, key: StringArg<'_>) -> Result<Self::MapStringKey> {
            self.ctx.write_string(key).map(SimpleString)
        }
    }

    impl<'a, W: Write> ser::IntMapSer for Compound<'a, W> {
        fn serialize_key(&mut self, key: i64) -> Result<()> {
            self.ctx.write_byte(chars::Separator)?;
            self.ctx.write_int(key)
        }
    }

    impl<'a, W: Write> ser::ObjMapSer for Compound<'a, W> {
        fn serialize_key<'ser, S, K: HaxeSerialize<'ser, S> + ?Sized>(
            &mut self,
            state: S,
            key: &'ser K,
        ) -> Result<()> {
            key.serialize(state, &mut *self.ctx)
        }
    }

    impl<'a, W: Write> ser::NamedSer for Compound<'a, W> {
        fn name(&mut self) -> StringRef {
            self.name
        }
    }

    impl<'a, W: Write> ser::NamedVariantSer for Compound<'a, W> {
        fn variant_name(&mut self) -> StringRef {
            self.variant_name
        }
    }
}

fn url_encode(buf: &mut Vec<u8>, s: &str) {
    buf.reserve(s.len());
    for c in s.as_bytes() {
        if let Some(c) = chars::url_encode(*c) {
            buf.push(c)
        } else {
            let (high, low) = chars::to_hex_digits(*c);
            buf.extend_from_slice(&[chars::URL_ESCAPE, high, low])
        }
    }
}

fn base64_len(bytes: &[u8]) -> usize {
    let bits = bytes.len().checked_mul(4).expect("overflow");
    bits / 3 + if bits % 3 == 0 { 0 } else { 1 }
}

fn write_base64<W: Write>(writer: &mut W, bytes: &[u8]) -> io::Result<()> {
    use super::chars::base64_encode as chr;

    let len = bytes.len() - bytes.len() % 3;
    for chunk in bytes[..len].chunks(3) {
        let (b1, b2, b3) = match chunk {
            [b1, b2, b3] => (b1, b2, b3),
            _ => unreachable!(),
        };

        writer.write_all(&[
            chr(b1 >> 2),
            chr((b1 << 4 | b2 >> 4) & 63),
            chr((b2 << 2 | b3 >> 6) & 63),
            chr(b3 & 63),
        ])?;
    }

    match bytes[len..] {
        [] => Ok(()),
        [b1] => writer.write_all(&[chr(b1 >> 2), chr((b1 << 4) & 63)]),
        [b1, b2] => writer.write_all(&[
            chr(b1 >> 2),
            chr((b1 << 4 | b2 >> 4) & 63),
            chr((b2 << 2) & 63),
        ]),
        _ => unreachable!(),
    }
}
